<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.png">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" type="text/css">
    <link href="/css/style.css" rel="stylesheet" type="text/css">
    <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/css/fileinput.min.css" rel="stylesheet" type="text/css">
    <link href="/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
    <link href="/css/bootstrap-wysiwyg.min.css" rel="stylesheet" type="text/css">
    <link href="/css/profession-black-green.css" rel="stylesheet" type="text/css" id="style-primary">
    <link href="/css/blocks.css" rel="stylesheet" type="text/css">
    <link href="/css/app.css" rel="stylesheet" type="text/css">
    <link href="/css/page_about.css" rel="stylesheet" type="text/css">
    <link href="/css/line-icons.css" rel="stylesheet" type="text/css">
    <!-- <link href="/css/styleUnify.css" rel="stylesheet" type="text/css"> -->
     <link href="/css/hexagons.css" rel="stylesheet" type="text/css">
    <link href="/css/hexagons.min.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/x-icon" href="assets/favicon.png">

    <title>Humaeck | Job Portal</title>
</head>


<body class="hero-content-dark footer-dark">

<div class="page-wrapper">
    <div class="header-wrapper">
    <div class="">
        <div class="header-top">
            <div class="container">
                <div class="header-brand">
                    <div class="header-logo">
                        <a href="/">
                            <img src="/images/logo-2.png" alt="Logo">
                        </a>
                    </div><!-- /.header-logo-->
                </div><!-- /.header-brand -->

                <!-- <ul class="header-actions nav nav-pills">
                    <li><a href="javascript:void(0);">Login</a></li>
                    <li><a href="javascript:void(0);">Sign Up</a></li>
                    <li><a href="javascript:void(0);" class="primary">Create Resume</a></li>
                </ul> --><!-- /.header-actions -->

                <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".header-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div><!-- /.container -->
        </div><!-- /.header-top -->

        <div class="header-bottom">
            <div class="container">
                <ul class="header-nav nav nav-pills collapse">
                    <li>
                        <a href="/">HOME</a>
                    </li>

                    <li class="active">
                        <a href="about-us">ABOUT US <i class=""></i></a>
                    </li>

                    <li >
                        <a href="services">SERVICES <i class=""></i> </a>
                    </li>

                    <li >
                        <a href="contact-us">CONTACT US <i class=""></i></a>
                        <!-- <ul class="sub-menu">
                            <li><a href="candidates.html">Candidates List</a></li>
                            <li><a href="resume.html">Resume</a></li>
                            <li><a href="create-resume.html">Create Resume</a></li>
                        </ul> --><!-- /.sub-menu -->
                    </li>
                </ul>

                <!-- <div class="header-search hidden-sm">
                    <form method="get" action="?">
                        <input type="text" class="form-control" placeholder="Search ...">
                    </form>
                </div> --><!-- /.header-search -->
            </div><!-- /.container -->
        </div><!-- /.header-bottom -->
    </div><!-- /.header -->
</div><!-- /.header-wrapper-->


   <div class="headerBackground text-center">
      <div class="container">
         <div class="row">
            <h1 class="headerText">About Us</h1>
         </div>
      </div><!--/end container-->
   </div>


   <div class="container-fluid">
      <div class="col-md-12">
         <div class="shadow-wrapper">
            <blockquote class="hero box-shadow shadow-effect-2">
               <h4 class="text-center">
                  <em>“Human resources are like natural resources; they're often buried deep. You have to go looking for them; they're not just lying around on the surface. You have to create the circumstances where they show themselves.”</em>
               </h4>
               <small class="pull-right"><em> Ken Robinson</em></small>
               <hr>
               <hr class="devider devider-db-dashed">
               <p align="justify">
               The future of any company lies with its team, better the team, better is the work. But hiring just the right person every time is not so easy for everyone. Hence, the recruitment procedure is very crucial; which means, any company that wants to hire a professional for core business, must take care of all the aspects while hiring, or else, it could cost a lot, in terms of time and money.
               </p>
               <p align="justify">
               Often, this leads to the establishment of HR department to recruit the team, which is, sometimes not even required by the company. Result? Extra-expenditure of money and time on the recruitment process, which is justified well, but not required... So, keeping an HR team is costlier; and outsourcing all recruitments could prove to be an absolute splurge.
               </p>
               <p align="justify">
               To help businesses out of this arduous cycle, Humaeck is coming in the market with a pro-active approach in the field of mass recruitment. Once an individual/company subscribes to our services, they can get the whole team for their business, verified and recruited by us, instead of going through the rigorous process of recruiting and training candidates for each post.
               </p>
               <p align="justify">
               We provide a customized solution to companies by mass recruiting for their team, thus providing our clients a huge cost benefit. Not only it helps clients, it also helps the talented professionals to quickly get hired in top companies. We also provide end to end support to our clients as well as the candidates.
               </p>
            </blockquote>
         </div>
      </div>
   </div>
   <div class="parallax-bg parallaxBg1" style="background-position: 50% 37px;">
         <div class="container content parallax-about">
            <div class="title-box-v2">
               <h2>About our <span class="color-orange">company</span></h2>
            </div>

            <div class="row">
               <div class="col-md-6">
                  <div class="banner-info dark margin-bottom-10">
                     <i class="rounded-x icon-bell"></i>
                     <div class="overflow-h">
                        <h3>Our mission</h3>
                        <p align="justify">Our mission is to make a strong and convenient HR recruitment system for our clients by bringing talented candidates and train them for best-suited companies.</p>
                     </div>
                  </div>
                  <div class="banner-info dark margin-bottom-10">
                     <i class="rounded-x fa fa-magic"></i>
                     <div class="overflow-h">
                        <h3>Our vision</h3>
                        <p align="justify">Our vision is to be a successful leader in the recruitment industry and take our innovative methods of work to every corner of the world.</p>
                     </div>
                  </div>
                  <div class="banner-info dark margin-bottom-10">
                     <i class="rounded-x fa fa-thumbs-o-up"></i>
                     <div class="overflow-h">
                        <h3>Core Values</h3>
                        <p align="justify">We believe that only true hard work and result-oriented approach can take us to the height of success and for that, we have hand-picked each member of our team. We innovative approach in each of our project and provide customized solutions.</p>
                     </div>
                  </div>
                  <div class="margin-bottom-20"></div>
               </div>
               <div class="col-md-6">
                  <img class="img-responsive" src="/images/BB.png" alt="">
               </div>
            </div>
         </div><!--/container-->
      </div>

    <div class="footer-wrapper">
    <div class="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="footer-top-block">
                            <h2>Humaeck</h2>
                          <p class="text-justify">
                              “Human resources are like natural resources; they're often buried deep. You have to go looking for them; they're not just lying around on the surface. You have to create the circumstances where they show themselves.”
                           </p>
                           <medium class="pull-right quote"><em> Ken Robinson</em></medium>
                            <!-- <ul class="social-links">
                                <li><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="javascript:void(0);"><i class="fa fa-tumblr"></i></a></li>
                            </ul> -->
                        </div><!-- /.footer-top-block -->
                    </div><!-- /.col-* -->

                    <div class="col-sm-3 col-sm-offset-1">
                        <div class="footer-top-block">
                            <h2>QUICK LINKS</h2>
                             <ul>
                                <li><a href="/" target="_blank">HOME</a></li>
                                <li><a href="about-us" target="_blank">ABOUT US</a></li>
                                <li><a href="services" target="_blank">SERVICES</a></li>
                                <li><a href="contact-us" target="_blank">CONTACT US</a></li>
                            </ul>
                        </div><!-- /.footer-top-block -->
                    </div><!-- /.col-* -->

                    <div class="col-sm-3">
                         <div class="footer-top-block">
                          <h2>SOCIAL NETWORK</h2>
                            <a href="https://www.facebook.com/" target="_blank" class="hb-xs-margin"><span class="hb hb-xs hb-facebook"><i class="fa fa-facebook"></i></span></a>
                            <a href="https://plus.google.com/" class="hb-xs-margin" target="_blank"><span class="hb hb-xs hb-google-plus"><i class="fa fa-google-plus"></i></span></a>
                            <a href="https://twitter.com/" class="hb-xs-margin" target="_blank"><span class="hb hb-xs hb-twitter"><i class="fa fa-twitter"></i></span></a>
                            <a href="https://www.linkedin.com/" class="hb-xs-margin" target="_blank"><span class="hb hb-xs hb-linkedin"><i class="fa fa-linkedin-square"></i></span></a>
                        </div><!-- /.footer-top-left -->
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.footer-top -->

        <div class="footer-bottom">
            <div class="container">
                <div class="footer-bottom-left">
                    &copy; <a href="javascript:void(0);">Humaeck</a>, 2017 All rights reserved.
                </div><!-- /.footer-bottom-left -->

                <div class="footer-bottom-right">
                    Created by <a href="http://technochords.com" target="blank">Technochords</a>
                </div><!-- /.footer-bottom-right -->
            </div><!-- /.container -->
        </div><!-- /.footer-bottom -->
    </div><!-- /.footer -->
</div><!-- /.footer-wrapper -->

</div><!-- /.page-wrapper -->


<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.ezmark.js"></script>

<script type="text/javascript" src="/js/collapse.js"></script>
<script type="text/javascript" src="/js/dropdown.js"></script>
<script type="text/javascript" src="/js/tab.js"></script>
<script type="text/javascript" src="/js/transition.js"></script>
<script type="text/javascript" src="/js/fileinput.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-wysiwyg.min.js"></script>
<script type="text/javascript" src="/js/jquery.cycle2.min.js"></script>
<script type="text/javascript" src="/js/jquery.cycle2.carousel.min.js"></script>
<script type="text/javascript" src="/js/countup.min.js"></script>
<script type="text/javascript" src="/js/profession.js"></script>
<script type="text/javascript" src="/js/hexagons.js"></script>
<script type="text/javascript" src="/js/hexagons.min.js"></script>

</body>
</html>
