<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.png">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" type="text/css">
    <link href="/css/style.css" rel="stylesheet" type="text/css">
    <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/css/fileinput.min.css" rel="stylesheet" type="text/css">
    <link href="/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
    <link href="/css/bootstrap-wysiwyg.min.css" rel="stylesheet" type="text/css">
    <link href="/css/profession-black-green.css" rel="stylesheet" type="text/css" id="style-primary">
     <link href="/css/hexagons.css" rel="stylesheet" type="text/css">
    <link href="/css/hexagons.min.css" rel="stylesheet" type="text/css">
    <link href="/css/sky-forms.css" rel="stylesheet" type="text/css">
    <link href="/css/custom-sky-forms.css" rel="stylesheet" type="text/css">

    <link rel="shortcut icon" type="image/x-icon" href="assets/favicon.png">

    <title>Humaeck | Job Portal</title>
</head>


<body class="hero-content-dark footer-dark">

<div class="page-wrapper">
    <div class="header-wrapper">
    <div class="">
        <div class="header-top">
            <div class="container">
                <div class="header-brand">
                    <div class="header-logo">
                        <a href="/">
                            <img src="/images/logo-2.png" alt="Logo">
                        </a>
                    </div><!-- /.header-logo-->
                </div><!-- /.header-brand -->

                <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".header-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div><!-- /.container -->
        </div><!-- /.header-top -->

        <div class="header-bottom">
            <div class="container">
                <ul class="header-nav nav nav-pills collapse">
                    <li class="">
                        <a href="/">HOME</a>
                    </li>

                    <li >
                        <a href="about-us">ABOUT US <i class=""></i></a>
                    </li>

                    <li >
                        <a href="services">SERVICES <i class=""></i> </a>
                    </li>

                    <li class="active">
                        <a href="contact-us">CONTACT US <i class=""></i></a>
                    </li>
                </ul>

               <!--  <div class="header-search hidden-sm">
                    <form method="get" action="?">
                        <input type="text" class="form-control" placeholder="Search ...">
                    </form>
                </div> --><!-- /.header-search -->
            </div><!-- /.container -->
        </div><!-- /.header-bottom -->
    </div><!-- /.header -->
</div><!-- /.header-wrapper-->


<div class="container">
    <div class="row margin40">
        <div class="col-md-6">
            <div class="well well-sm">
                <form class="form-horizontal" method="post" action="mailcontact.php">
                    <fieldset>
                        <legend class="text-center header">Contact us</legend>

                        <div class="sky-form">
                            <section>
                                <div class="inline-group">
                                    <label class="radio-inline checkboxClient">
                                        <input type="radio" name="checkboxclientcandidate" id="checkboxClick" value="1" checked=""><i class="rounded-x"></i>Client
                                    </label>

                                   <label class="radio-inline checkboxCandidate">
                                        <input type="radio" name="checkboxclientcandidate" id="checkboxClick1" value="0"><i class="rounded-x"></i> Candidate
                                    </label>
                                </div>
                            </section>
                        </div>

                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                <input id="firstname" name="firstname" type="text" placeholder="First Name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                <input id="lastname" name="lastname" type="text" placeholder="Last Name" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                <input id="email" name="email" type="text" placeholder="Email Address" class="form-control" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                <input id="phone" name="phone" type="text" placeholder="Phone" class="form-control">
                            </div>
                        </div>

                        <div class="workingBlock hide">

                             <div class="col-md-10 col-md-offset-1">
                                <div class="form-group">
                                    <input maxlength="200" type="text" class="form-control" name="specialization" placeholder="Enter specialization" />
                                </div>
                            </div>
                            <div class="col-md-10 col-md-offset-1">
                                <div class="form-group">
                                    <input maxlength="200" type="text" class="form-control" name="Qualification" placeholder="Enter Highest Qualification" />
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                <textarea class="form-control" id="message" name="message" placeholder="Enter your massage for us here. We will get back to you within 2 business days." rows="7"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" value="Submit" class="btn btn-primary btn-lg">Submit</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="col-md-6">
            <div>
                <div class="panel panel-default">
                    <div class="text-center header">Our Office</div>
                    <div class="panel-body text-center">
                        <h4>Address</h4>
                        <div>
                        404, Vishal Bhawan 95, Nehru Place<br />
                        New Delhi - 110019, India<br />
                        +91 (11) 6547 4737 / +91 - 7982 17 6224<br />
                        hrsupport@humaeck.com<br />
                        </div>
                        <hr />
                        <div id="map" class="map">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="footer-wrapper">
    <div class="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="footer-top-block">
                            <h2>Humaeck</h2>
                         <p class="text-justify">
                              “Human resources are like natural resources; they're often buried deep. You have to go looking for them; they're not just lying around on the surface. You have to create the circumstances where they show themselves.”
                           </p>
                           <medium class="pull-right quote"><em> Ken Robinson</em></medium>
                        </div><!-- /.footer-top-block -->
                    </div><!-- /.col-* -->

                    <div class="col-sm-3 col-sm-offset-1">
                        <div class="footer-top-block">
                            <h2>QUICK LINKS</h2>
                             <ul>
                                <li><a href="/" target="_blank">HOME</a></li>
                                <li><a href="about-us" target="_blank">ABOUT US</a></li>
                                <li><a href="services" target="_blank">SERVICES</a></li>
                                <li><a href="contact-us" target="_blank">CONTACT US</a></li>
                            </ul>
                        </div><!-- /.footer-top-block -->
                    </div><!-- /.col-* -->

                    <div class="col-sm-3">
                         <div class="footer-top-block">
                          <h2>SOCIAL NETWORK</h2>
                            <a href="https://www.facebook.com/" target="_blank" class="hb-xs-margin"><span class="hb hb-xs hb-facebook"><i class="fa fa-facebook"></i></span></a>
                            <a href="https://plus.google.com/" class="hb-xs-margin" target="_blank"><span class="hb hb-xs hb-google-plus"><i class="fa fa-google-plus"></i></span></a>
                            <a href="https://twitter.com/" class="hb-xs-margin" target="_blank"><span class="hb hb-xs hb-twitter"><i class="fa fa-twitter"></i></span></a>
                            <a href="https://www.linkedin.com/" class="hb-xs-margin" target="_blank"><span class="hb hb-xs hb-linkedin"><i class="fa fa-linkedin-square"></i></span></a>
                        </div><!-- /.footer-top-left -->
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.footer-top -->

        <div class="footer-bottom">
            <div class="container">
                <div class="footer-bottom-left">
                    &copy; <a href="javascript:void(0);">Humaeck</a>, 2017 All rights reserved.
                </div><!-- /.footer-bottom-left -->

                <div class="footer-bottom-right">
                    Created by <a href="http://technochords.com" target="blank">Technochords</a>
                </div><!-- /.footer-bottom-right -->
            </div><!-- /.container -->
        </div><!-- /.footer-bottom -->
    </div><!-- /.footer -->
</div><!-- /.footer-wrapper -->

</div><!-- /.page-wrapper -->

<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.ezmark.js"></script>

<script type="text/javascript" src="/js/collapse.js"></script>
<script type="text/javascript" src="/js/dropdown.js"></script>
<script type="text/javascript" src="/js/tab.js"></script>
<script type="text/javascript" src="/js/transition.js"></script>
<script type="text/javascript" src="/js/fileinput.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-wysiwyg.min.js"></script>
<script type="text/javascript" src="/js/jquery.cycle2.min.js"></script>
<script type="text/javascript" src="/js/jquery.cycle2.carousel.min.js"></script>
<script type="text/javascript" src="/js/countup.min.js"></script>
<script type="text/javascript" src="/js/profession.js"></script>

<script type="text/javascript" src="/js/hexagons.js"></script>
<script type="text/javascript" src="/js/hexagons.min.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrK-iY8CjtSXKTi-rznGmX-BA5h1hqhOY&callback=initMap"></script>
<script>
      function initMap() {
        var mylocation = {lat: 28.5481742, lng: 77.2520583};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: mylocation
        });
        var marker = new google.maps.Marker({
          position: mylocation,
          map: map
        });
      }
    </script>
    
<script type="text/javascript">
$('#checkboxClick').on('change', function(){
            if( '.checkboxCandidate' ){
                $('.workingBlock').addClass('hide');
            }else{
                $('.workingBlock').removeClass('hide');
            }
        });


        $('#checkboxClick1').on('change', function(){
            if( '.checkboxClient' ){
                $('.workingBlock').removeClass('hide');
            }else{
                $('.workingBlock').addClass('hide');
            }
        });
</script>

</body>
</html>
