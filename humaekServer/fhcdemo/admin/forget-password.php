<?php 
require_once('../configuration/configuration.php');

$oGeneral = new GeneralClass();
$oAdmin   = new AdminClass();
if(isset($_POST['submit']))
{
    $bData = $_POST;
$id = $oGeneral->get_records('tbl_admin','fld_id','fld_email',$bData['fld_email']);
  if(!$id['fld_id']){$msg = 'Your Email is not valid';}else{

   $pwd=$aData['fld_password'] = rand(10000,999999);
    $oGeneral->update_data('tbl_admin','fld_id',$aData,$id);
    $msg = 'Your Password  is sent to your mail.';

    $toemail = $bData['fld_email'];
$strMessage    .='<table width="80%" border="1" align="center" cellpadding="8" cellspacing="0" bordercolor="#E8E8E8" bgcolor="#FBFBFB" style="border-collapse:collapse" class="formtxt">
     <tr>
      <td height="50" colspan="2" bgcolor="#EEEEEE" align="center"><strong style="font-size:15px; color:#000000">Your Password</strong></td>
    </tr>
   
    <tr>
      <td width="26%"><strong>Email</strong></td>
      <td>'.$toemail.'</td>
    </tr>
    <tr>
      <td width="26%"><strong>Password</strong></td>
      <td>'.$pwd.'</td>
    </tr>
    <tr>
      <td width="26%"><strong>Date</strong></td>
      <td>'.date('d-m-Y h:i:s').'</td>
    </tr>

   </table>';

$strSubject    =    "Your Password";
$email = "noreply@webcountry.in";
// To send HTML mail, the Content-type header must be set
$header = "From: ". $_POST['fld_email']. " <" . $email . ">\r\n";

$header .= 'Content-type: text/html; charset=UTF-8'."\r\n";

mail($toemail, $strSubject, $strMessage, $header);
}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
		<meta name="author" content="Coderthemes">

		<link rel="shortcut icon" href="assets/images/favicon_1.ico">

		<title>Responsive Admin Dashboard</title>

		<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

	</head>
	<body>

		<div class="account-pages"></div>
		<div class="clearfix"></div>
		<div class="wrapper-page">
			<div class=" card-box">
				<div class="panel-heading">
					<h3 class="text-center"> Reset Password </h3>
					<center><h5><?=($msg)?$msg:'';?></h5></center>
				</div>

				<div class="panel-body">
					<form method="post" action="#" role="form" class="text-center">
						<div class="alert alert-info alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
								×
							</button>
							Enter your <b>Email</b> and instructions will be sent to you!
						</div>
						<div class="form-group m-b-0">
							<div class="input-group">
								<input type="email" class="form-control" name="fld_email" placeholder="Enter Email" required="">
								<span class="input-group-btn">
									<button type="submit" class="btn btn-purple w-sm waves-effect waves-light">
										Reset
									</button> 
								</span>
							</div>
						</div>
					<div class="col-sm-6" style="padding:0px ;padding-top:10px; text-align:left">
                        <a href="index.php" class="text-dark"><i class="fa fa-lock m-r-5"  style="text-align:left"></i> Login Now</a>
                    </div>
					<div class="col-sm-6">
                       
                    </div>

					</form>
				</div>
			</div>
			

		</div>

		<script>
			var resizefunc = [];
		</script>

		<!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>


        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

	</body>
</html>