<?php include('../configuration/configuration.php');

$oUser = new UserClass();
$oUser->getAbout();
$iAbout  = $oUser->iResults;
	
 ?> 

<!DOCTYPE html>

<html>

    <head>

        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="description" content="">

        <meta name="author" content="Coderthemes">



        <link rel="shortcut icon" href="assets/images/feb.ico">



        <title>FHC - Job Portal</title>



        <!--Morris Chart CSS -->

		<link rel="stylesheet" href="assets/plugins/morris/morris.css">



        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />

        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />

        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />

        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />

        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />



        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->

        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

        <!--[if lt IE 9]>

        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

        <![endif]-->



        <script src="assets/js/modernizr.min.js"></script>





    </head>





    <body class="fixed-left">



        <!-- Begin page -->

        <div id="wrapper">



            <!-- Top Bar Start -->

     <div class="topbar">



                <!-- LOGO -->

                <div class="topbar-left">

                    <div class="text-center">

                        <a href="dashboard.php" class="logo"><i class="icon-fhc icon-c-logo"></i><span><img src="assets/images/cologo.png"></span></a>

                        <!-- Image Logo here -->

                        <!--<a href="index.html" class="logo">-->

                            <!--<i class="icon-c-logo"> <img src="assets/images/logo_sm.png" height="42"/> </i>-->

                            <!--<span><img src="assets/images/logo_light.png" height="20"/></span>-->

                        <!--</a>-->

                    </div>

                </div>



                <!-- Button mobile view to collapse sidebar menu -->

                <div class="navbar navbar-default" role="navigation">

                    <div class="container">

                        <div class="">

                           <div class="pull-left">

                                <?php /*?><button class="button-menu-mobile open-left waves-effect waves-light">

                                    <i class="md md-menu"></i>

                                </button><?php */?>

                                <span class="clearfix"></span>

                            </div>





                            <?php /*?><form role="search" class="navbar-left app-search pull-left hidden-xs">

			                     <input type="text" placeholder="Search..." class="form-control">

			                     <a href=""><i class="fa fa-search"></i></a>

			                </form><?php */?>





                            <ul class="nav navbar-nav navbar-right pull-right">

                                <li class="dropdown top-menu-item-xs">

                                    <a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">

                                       

                                    </a>

                                  

                                </li>

                                <?php /*?><li class="hidden-xs">

                                    <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>

                                </li>

                                

                                <li class="dropdown top-menu-item-xs"><?php */?>

                                    <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><img src="assets/images/users/avatar-1.jpg" alt="user-img" class="img-circle"> </a>

                                    <ul class="dropdown-menu">

                                        <li><a href="javascript:void(0)"><i class="ti-user m-r-10 text-custom"></i> Profile</a></li>

                                       <!-- <li><a href="javascript:void(0)"><i class="ti-settings m-r-10 text-custom"></i> Settings</a></li>

                                        <li><a href="javascript:void(0)"><i class="ti-lock m-r-10 text-custom"></i> Lock screen</a></li>

                                        --><li class="divider"></li>

                                        <li><a href="logout.php"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>

                                    </ul>

                                </li>

                            </ul>

                        </div>

                        <!--/.nav-collapse -->

                    </div>

                </div>

            </div>      

            <!-- Top Bar End -->



<div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>
							
                        	<li class="text-muted menu-title">Navigation</li>
						<?php if(@$_SESSION['ADMIN_T']==1){ ?>
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-home"></i> <span> Employee Details </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="manage-employee.php">Employee Information</a></li>                                   
                                </ul>
                            </li>
						<?php } ?>							
							
                            
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-home"></i> <span> Manage Pages </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                   <li><a href="add-services.php">Add Services Content</a></li> 
                                    <li><a href="manage-services.php">Manage Services </a></li>
									<?php if($iAbout > 0){ ?>
									<li><a href="add-about.php" class="not-active">Add About Content</a></li> 
									<?php }else{ ?>
									<li><a href="add-about.php">Add About Content</a></li> 	
									<?php  } ?>									
                                    <li><a href="manage-about.php">Manage About us</a></li>                                  
                                </ul>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    
                    
                    
                    
                    <div class="clearfix"></div>
                </div>
            </div>

            <!-- ========== Left Sidebar Start ========== -->



          