<?php include('../configuration/configuration.php');

$oCompany_details = new AdminClass();

$oGeneral = new GeneralClass();

$viewID = $_REQUEST['view-id'];

$oCompany_details->get_emp_details($viewID);

$aEmployee_details  = $oCompany_details->aResults;

$iEmployee_details  = $oCompany_details->iResults;	

	//print_r($aEmployee_details);

?>





<div class="modal-header">

	<a class="close" data-dismiss="modal">&times;</a>

	<h3 style="text-align:center;">View Employee Detail</h3>

</div>

<div class="modal-body" id="viewemp">

	<table>

	<tr>		

		<td>Employee Name</td> 

		<td><?=$aEmployee_details[0]['name']?></td>	

	</tr>

	<tr><td>Email ID</td> 

		<td><?=$aEmployee_details[0]['email']?></td>	

	</tr><tr>		

		<td>Password</td> 

		<td><?=$aEmployee_details[0]['password']?></td>	

	</tr><tr>		

		<td>Mobile number</td> 

		<td><?=$aEmployee_details[0]['phone']?></td>	

	</tr><tr>		

		<td>Total Work Experience</td> 

		<td><?=$aEmployee_details[0]['work_exp_year']?> Year <?=$aEmployee_details[0]['work_exp_month']?> Month</td>	

	</tr><tr>		

		<td>Resume</td> 

		<td><a href="../images/<?=$aEmployee_details[0]['resume']?>" download data-toggle="tooltip" data-placement="top" title="Download Resume" target="_blank" width="150px" height="150px">Download Resume</a></td>	

	</tr><tr>		

		<td>Current Designation</td> 

		<td><?=$aEmployee_details[0]['current_designation']?></td>	

	</tr><tr>		

		<td>Current Company</td> 

		<td><?=$aEmployee_details[0]['current_company']?></td>	

	</tr><tr>		

		<td>Annual Salary</td> 

		<td><?=$aEmployee_details[0]['annual_salary_lakhs']?> Lakhs <?=$aEmployee_details[0]['annual_salary_thousand']?> Thousand</td>	

	</tr><tr>		

		<td>Working since</td> 

		<td><?=$aEmployee_details[0]['working_since_year']?>/<?=$aEmployee_details[0]['working_since_month']?> To <?=$aEmployee_details[0]['working_since_present']?></td>	

	</tr><tr>		

		<td>Current Location</td> 

		<td><?=$aEmployee_details[0]['fld_state_name']?>,<?=$aEmployee_details[0]['fld_city_name']?></td>	

	</tr><tr>		

		<td>Duration of Notice Period</td> 

		<td><?=$aEmployee_details[0]['duration_notice_period']?></td>	

	</tr><tr>		

		<td>Skills</td> 

		<td><?=$aEmployee_details[0]['skills']?></td>	

	</tr><tr>		

		<td>Highest Qualification</td> 

		<td><?=$aEmployee_details[0]['highest_qualification']?></td>	

	</tr><tr>		

		<td>Course</td> 

		<td><?=$aEmployee_details[0]['course_name']?></td>	

	</tr><tr>		

		<td>Specialization</td> 

		<td><?=$aEmployee_details[0]['specialization']?></td>	

	</tr><tr>		

		<td>University/College</td> 

		<td><?=$aEmployee_details[0]['university_name']?></td>	

	</tr><tr>		

		<td>Course Duration</td> 

		<td><?=$aEmployee_details[0]['from_date']?></td>To<td><?=$aEmployee_details[0]['to_date']?></td>

	</tr><tr>		

		<td>Passing Year</td> 

		<td><?=$aEmployee_details[0]['passing_year']?></td>	

	</tr>

	</table>

</div>

<div class="modal-footer">

	<a class="btn" data-dismiss="modal">Close</a>

</div>