<?php include('../configuration/configuration.php');

$oCompany_details = new AdminClass();

$oGeneral = new GeneralClass();

$oCompany_details->get_emp_details();

$aEmployee_details  = $oCompany_details->aResults;

$iEmployee_details  = $oCompany_details->iResults;	

	//print_r($aEmployee_details);

?>

    <script>

    function removeList(id)

	{

		 var x= confirm("Are you sure delete?");

			 if (x==true) {

			$("#row"+id).remove();											

			$.post( "delete-employee.php?id="+id, function( data ) {

					$('.vd_hidden').css('display','block');

					$('.vd_hidden').html(data);				



	});

	}else{return false;}

		

	}

    </script>

<?php require_once('include/header.php'); ?> 



<section id="main-content">

<section class="wrapper">



<div class="row">

            <div class="col-sm-12">

                <section class="panel">                   

                    <span class="vd_hidden delrecord" style="display:none;"></span>

					<h3 style="text-align:center;">EMPLOYEES DETAILS</h3>

                    <div class="panel-body">					

                    <center><span><?=($msgA)?'Records are '.$msgA.'successfully':'';?></span></center>

					

                        <table class="table  table-hover general-table">

                            <thead>

                            <tr><th>Serial No.</th>

                                <th>Name</th>

                                <th>Email</th>

                                <th>Mobile</th>

                                <th>Resume</th>

								<th>Status</th>   								

                                <th>View</th>                                                              

                                                             

                                <td>Action</td>

                               

                            </tr>

                            </thead>

                            <form name="_frmManagePages" method="post">

                            <input type="hidden" name="_hide" id="_hide" value="">

                            <tbody>

                            <?php 

							if($iEmployee_details > 0){

								$k=0;

							for($i=0;$i < $iEmployee_details;$i++){ $k++;  ?>							

                            <tr id="row<?=$aEmployee_details[$i]['id']?>">

								<td><?php echo $k; ?></td>

                                <td><?=$aEmployee_details[$i]['name']?></td> 

                                <td><?=$aEmployee_details[$i]['email']?></td> 

                                <td><?=$aEmployee_details[$i]['phone']?></td> 

                                <td><a href="../images/<?=$aEmployee_details[$i]['resume']?>" download data-toggle="tooltip" data-placement="top" title="Download Resume" target="_blank" width="150px" height="150px"><i class="glyphicon glyphicon-download"></i></a></td>                               

                                <td>

                                <span class="btn menu-icon  vd_bd-grey vd_grey"><a href="update-status.php?id=<?=$aEmployee_details[$i]['id']?>&status=<?=$aEmployee_details[$i]['status']?>" data-toggle="tooltip" data-placement="top" title="Change Status"><?=($aEmployee_details[$i]['status']==1)?'<i class="fa fa-circle aactive" aria-hidden="true"></i>

</i>

':'<i class="fa fa-circle inactive" aria-hidden="true"></i>

';?></a></span>

                                </td>

								

                                <td><a href="view-employee.php?view-id=<?=$aEmployee_details[$i]['id']?>" class="" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye"></i>

								</a>

								</td>

								

                                <td><a href ="#" class="menu-icon  vd_bd-grey vd_grey" onClick="removeList(<?=$aEmployee_details[$i]['id']?>);" data-toggle="tooltip" data-placement="top" title="Delete Record"><i class="fa fa-times"></i></a></td>

                            </tr>

                            <?php } } else { ?>

                            <tr><td colspan="6" align="center"> No Record found.</td></tr>

                            <?php } ?>

                            </tbody>

                            </form>

                        </table>

                    </div>

                </section>

            </div>

        </div>

</section>

</section>

<?php unset($_SESSION['amsg']);?>



<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->

 <?php require_once('include/footer.php'); ?>  

<!-- Modal -->

<!-- Modal HTML -->

    <div id="myModal" class="modal fade">

        <div class="modal-dialog">

            <div class="modal-content">

                <!-- Content will be loaded here from "remote.php" file -->

            </div>

        </div>

    </div>