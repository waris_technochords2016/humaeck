<?php include('../../configuration/configuration.php');

$oUser = new UserClass();
$oUser->getAbout();
$iAbout  = $oUser->iResults;
	
 ?>  
  <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>
							
                        	<li class="text-muted menu-title">Navigation</li>
						<?php if(@$_SESSION['ADMIN_T']==1){ ?>
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-home"></i> <span> Employee Details </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="manage-employee.php">Employee Information</a></li>                                   
                                </ul>
                            </li>
						<?php } ?>							
							
                            
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-home"></i> <span> Manage Pages </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                   <li><a href="add-services.php">Add Services Content</a></li> 
                                    <li><a href="manage-services.php">Manage Services </a></li>
									<?php if($iAbout > 0){ ?>
									<li><a href="add-about.php" class="not-active">Add About Content</a></li> 
									<?php }else{ ?>
									<li><a href="add-about.php">Add About Content</a></li> 	
									<?php  } ?>									
                                    <li><a href="manage-about.php">Manage About us</a></li>                                  
                                </ul>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    
                    
                    
                    
                    <div class="clearfix"></div>
                </div>
            </div>