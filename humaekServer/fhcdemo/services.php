<?php include('configuration/configuration.php');

$oUser = new UserClass();

$oUser->getServices();

$aServices  = $oUser->aResults;

$iServices  = $oUser->iResults;	

	//print_r($aEmployee_details);

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!--   <link rel="icon" href="../../favicon.ico">-->
<title>fhcdemo</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/settings.css" rel="stylesheet">
<link href="css/styleslider.css" rel="stylesheet">
<link href="font-awesome/css/font-awesome.css" rel="stylesheet">
</head>
<body>
<?php include('header.php') ?>
<div class="headerbaner12">

</div>
<div class="container">
  <div class="toppading">
  <?php for($i=0;$i < $iServices;$i++){  
  if(($i+2)%2==1){ ?>
  <div class="servicessection2"> 
      <div class="nopading bgh col-md-6 col-md-push-6">
        <div class="grid">
          <figure class="effect-bubba"> <img src="admin/images/<?=$aServices[$i]['fld_image']?>" alt="img02"/>
            <figcaption>
              <h2><?=$aServices[$i]['fld_image_title']?></h2>
              <p><?=$aServices[$i]['fld_image_slogon']?></p>
            </figcaption>
          </figure>
        </div>
      </div>
      <div class="col-md-6 col-md-pull-6">
        <div class="servicescontent">
          <h3><?=$aServices[$i]['fld_title']?></h3>
          <?=$aServices[$i]['fld_content']?>
        </div>
      </div>
      
       
    </div>
 <?php } else { ?>
  <div class="clearfix"></div>
  <div class="servicessection1">
      <div class="col-md-6 col-sm-6 col-lg-6 bgh">
        <div class="grid">
          <figure class="effect-bubba"> <img src="admin/images/<?=$aServices[$i]['fld_image']?>" alt="img02"/>
            <figcaption>
              <h2><?=$aServices[$i]['fld_image_title']?></h2>
              <p><?=$aServices[$i]['fld_image_slogon']?></p>
            </figcaption>
          </figure>
        </div>
      </div>
      <div class="col-md-6 col-sm-6 col-lg-6">
        <div class="servicescontent">
        
        <h3><?=$aServices[$i]['fld_title']?></h3>
          <?=$aServices[$i]['fld_content']?>
        </div>
      </div>
    </div>
  
<?php } }  ?>
  </div>
</div>
<div class="clearfix"></div>
<?php include('footer.php') ?>
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/jquery.themepunch.plugins.min.js"></script>
<script>
var  mn = $(".main-nav");
    mns = "main-nav-scrolled";
    hdr = $('header').height();

$(window).scroll(function() {
  if( $(this).scrollTop() > hdr ) {
    mn.addClass(mns);
  } else {
    mn.removeClass(mns);
  }
});
</script>
<script type="text/javascript">

		var revapi;

		jQuery(document).ready(function() {

			   revapi = jQuery('.tp-banner').revolution(
				{
					delay:9000,
					startwidth:1170,
					startheight:500,
					hideThumbs:10,
					hideCaptionAtLimit:650,
					hideAllCaptionAtLimit:400



				});

		});

	</script>
</body>
</html>
