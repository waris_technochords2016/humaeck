<?php 
require_once('configuration/configuration.php');

$oUser = new UserClass();
$oUser->getAbout();

$aAbout  = $oUser->aResults;

$iAbout  = $oUser->iResults;

//print_r($aAbout);	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!--   <link rel="icon" href="../../favicon.ico">-->
<title>fhcdemo</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/settings.css" rel="stylesheet">
<link href="css/styleslider.css" rel="stylesheet">

<link href="font-awesome/css/font-awesome.css" rel="stylesheet">
</head>
<body>
<?php include('header.php') ?>
<div class="clearfix"></div>
<div class="demo-5">
  <div id="container" class="container intro-effect-sidefixed">
    <!-- Top Navigation -->
    <!--<div class="codrops-top clearfix">  <span class="right"><a class="codrops-icon codrops-icon-drop" href="#"><span>Back to the Codrops Article</span></a></span> </div>-->
    <div class="logoandcontent"><a class="navbar-brand bng" href="index.php"><img src="images/logo.png"> </a> </div>
    <header class="header">
      <div class="bg-img"><img src="admin/images/<?=$aAbout[0]['fld_image']?>" alt="Background Image"></div>
    </header>
    
    <div class="pandeytl" style=""></div>
    <article class="content">
      <div class="title">
        <!--<nav class="codrops-demos"> <a href="#">Push</a> <a href="#">Fade Out</a> <a href="#">Sliced</a> <a href="#">Side</a> <a class="current-demo" href="#">Fixed Side</a> <a href="#">Grid</a> <a href="#">Jam 3</a> </nav>-->
        <h1>
          
          <button class="trigger" data-info="<?=$aAbout[0]['fld_title']?>"></button>
        </h1>
        <p class="subline"></p>
      </div>
      <div class="abtcontent">
        <?=$aAbout[0]['fld_content']?>
      </div>
    </article>
  </div>
</div>
<!-- /container -->
<script src="js/classie.js"></script>
<script>
			(function() {

				// detect if IE : from http://stackoverflow.com/a/16657946		
				var ie = (function(){
					var undef,rv = -1; // Return value assumes failure.
					var ua = window.navigator.userAgent;
					var msie = ua.indexOf('MSIE ');
					var trident = ua.indexOf('Trident/');

					if (msie > 0) {
						// IE 10 or older => return version number
						rv = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
					} else if (trident > 0) {
						// IE 11 (or newer) => return version number
						var rvNum = ua.indexOf('rv:');
						rv = parseInt(ua.substring(rvNum + 3, ua.indexOf('.', rvNum)), 10);
					}

					return ((rv > -1) ? rv : undef);
				}());


				// disable/enable scroll (mousewheel and keys) from http://stackoverflow.com/a/4770179					
				// left: 37, up: 38, right: 39, down: 40,
				// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
				var keys = [32, 37, 38, 39, 40], wheelIter = 0;

				function preventDefault(e) {
					e = e || window.event;
					if (e.preventDefault)
					e.preventDefault();
					e.returnValue = false;  
				}

				function keydown(e) {
					for (var i = keys.length; i--;) {
						if (e.keyCode === keys[i]) {
							preventDefault(e);
							return;
						}
					}
				}

				function touchmove(e) {
					preventDefault(e);
				}

				function wheel(e) {
					// for IE 
					//if( ie ) {
						//preventDefault(e);
					//}
				}

				function disable_scroll() {
					window.onmousewheel = document.onmousewheel = wheel;
					document.onkeydown = keydown;
					document.body.ontouchmove = touchmove;
				}

				function enable_scroll() {
					window.onmousewheel = document.onmousewheel = document.onkeydown = document.body.ontouchmove = null;  
				}

				var docElem = window.document.documentElement,
					scrollVal,
					isRevealed, 
					noscroll, 
					isAnimating,
					container = document.getElementById( 'container' ),
					trigger = container.querySelector( 'button.trigger' );

				function scrollY() {
					return window.pageYOffset || docElem.scrollTop;
				}
				
				function scrollPage() {
					scrollVal = scrollY();
					
					if( noscroll && !ie ) {
						if( scrollVal < 0 ) return false;
						// keep it that way
						window.scrollTo( 0, 0 );
					}

					if( classie.has( container, 'notrans' ) ) {
						classie.remove( container, 'notrans' );
						return false;
					}

					if( isAnimating ) {
						return false;
					}
					
					if( scrollVal <= 0 && isRevealed ) {
						toggle(0);
					}
					else if( scrollVal > 0 && !isRevealed ){
						toggle(1);
					}
				}

				function toggle( reveal ) {
					isAnimating = true;
					
					if( reveal ) {
						classie.add( container, 'modify' );
					}
					else {
						noscroll = true;
						disable_scroll();
						classie.remove( container, 'modify' );
					}

					// simulating the end of the transition:
					setTimeout( function() {
						isRevealed = !isRevealed;
						isAnimating = false;
						if( reveal ) {
							noscroll = false;
							enable_scroll();
						}
					}, 600 );
				}

				// refreshing the page...
				var pageScroll = scrollY();
				noscroll = pageScroll === 0;
				
				disable_scroll();
				
				if( pageScroll ) {
					isRevealed = true;
					classie.add( container, 'notrans' );
					classie.add( container, 'modify' );
				}
				
				window.addEventListener( 'scroll', scrollPage );
				trigger.addEventListener( 'click', function() { toggle( 'reveal' ); } );
			})();
		</script>
<!-- For the demo ad only -->
<style>
body {
	background: #fff;
	
	line-height: 1.5;
	font-family: 'Raleway', Arial, sans-serif;
	text-rendering: optimizeLegibility;
	-webkit-font-smoothing: antialiased;
	overflow-x: hidden;
}
nav {
  background: #ffffff none repeat scroll 0 0 !important;
  margin: 0 !important;
  position: relative !important;
}

.navbar-brand.bng > img {
  display: none;
}
.navbar-fixed-top {
  position: fixed !important;
}
.title {
  margin-top: 22px !important;
}
.title {
  background: none!important;
}
.employee {
 
  margin-top: 17px;
}
</style>
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/jquery.themepunch.plugins.min.js"></script>
<script>
$("#container .pandeytl").click(function(){
  //alert('ssss');
  $("#container").removeClass('modify');
  //$("body").scrollTop();
   $("html, body").animate({
            scrollTop: 0
        }, 600);
        
    
    //$("p").hide();
});
</script>
</body>
</html>
