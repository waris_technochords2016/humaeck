<div class="footer">
  <div class="container">
    <div class="col-md-3 col-sm-3 col-lg-3 fot4">
      <div class="sectionft">
        <h4>About Us</h4>
        <!--<ul>
          <li><a href="#">What we do</a></li>
          <li><a href="#">Our values</a></li>
          <li><a href="#">Our team</a></li>
          <li><a href="#">Responsible business </a></li>
        </ul>-->
        <p>"Human resources are like natural resources; they're often buried deep. You have to go looking for them; they're not just lying around on the surface. You have to create the circumstances where they show themselves."</p>
      </div>
    </div>
    <div class="col-md-3 col-sm-3 col-lg-3 fot4">
      <div class="sectionft">
        <h4>Our Links</h4>
        <ul>
          <li><a href="index.php">Home</a></li>
          <li><a href="aboutus.php">About Us</a></li>
          <li><a href="services.php">Services</a></li>
          <li><a href="/blog">Blog</a></li>
          <li><a href="contactus.php">Contact Us</a></li>
        </ul>
      </div>
    </div>
    <div class="col-md-3 col-sm-3 col-lg-3 fot4">
      <div class="sectionft soc">
        <h4>Social Media</h4>
        <ul class="media">
          <li><a href="#"><img src="images/facebook-icon.png" /></a></li>
          <li><a href="#"><img src="images/go-icon.png" /></a></li>
          <li><a href="#"><img src="images/linkedin30x30.png" /></a></li>
          <li><a href="#"><img src="images/twitter-icon.png" /></a></li>
        
         
        </ul>
      </div>
    </div>
    <div class="col-md-3 col-sm-3 col-lg-3 fot4">
      <div class="sectionft vg">
        <h4>Contact Us</h4>
        <ul>
  <li><span class="addred">Address :</span> Folks Hunt Consulting
C/o. Regus Business Center
Level 3 Vasant Square Mall
Pocekt V, Sector B
Vasant Kunj
<li><span class="addred">City :</span> New Delhi</li>
</li>
  <li><span class="addred">Email</span> support@folkshunt.com</li>
  <li><span class="keasf">PIN Code :</span>  110070</li>
  <li><span class="addred">Contact Number : </span> +91116-648-1856</li>
  </ul>
      </div>
    </div>
    
  </div>
  <div class="clearfix"></div>
<div class="secodnfooter">
<p class="av">Copyright 2016 @ folkshuntconsulting.com All right reserved</p>
</div> 
</div>

