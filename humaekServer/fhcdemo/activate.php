<?php error_reporting(1);
require_once('configuration/configuration.php');
 $oUser = new UserClass();

$oAdmin = new AdminClass();

if(isset($_POST['submit']))
{
	
	$fld_login     =  $_POST['fld_email'];
	$fld_password  =  $_POST['fld_password'];

$oAdmin->get_emp_details('',$fld_login,1,$fld_password);
$aEmp = $oAdmin->aResults;
$iEmp = $oAdmin->iResults;
//print_r($aEmp );die;
if($iEmp > 0){
  $_SESSION['EMP_ID'] = $aEmp[0]['id'];
  $_SESSION['EMP_U']  = $aEmp[0]['email'];

   header('location:profile.php');
   }
   else
   {
  header('location:index.php?message=1');
  }
  }


if($_REQUEST['email']!='' && $_REQUEST['key']!='' ){
	 
	  $oUser->updateKey($_REQUEST['email'],$_REQUEST['key']);
      $aUser =$oUser->aResults;
	  header('location:index.php?message=1');
	  }
   
?>
<?php include('header.php'); ?>
    <body>
        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page loginpagehome">
        	<div class="card-box pnl">
            <div class="panel-heading"> 
                <h3 class="text-center"> Sign In<strong class="text-custom"></strong> </h3>
            </div> 


            <div class="panel-body">
            <form class="form-horizontal m-t-20" action="" method="POST">
                
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="email" required name="fld_email" placeholder="Email ID">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" required name="fld_password" placeholder="Password">
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-primary chehome">
                            <input id="checkbox-signup" type="checkbox">
                            <label for="checkbox-signup">
                                Remember me
                            </label>
                        </div>
                        
                    </div>
                </div>
                
                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-block text-uppercase waves-effect waves-light" type="submit" name="submit">Log In</button>
                    </div>
                </div>
				<div><?php if($_GET['message']==1){ echo "Email Or Password Incorrect";} ?></div>
                <div class="form-group m-t-30 m-b-0">
                    <div class="col-sm-6 foghome">
                        <a href="resetpassword.php" class="text-dark"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                    </div>
					<div class="col-sm-6 reghome">
                        <a href="registration.php" class="text-dark"><i class="fa fa-lock m-r-5"></i> Registration</a>
                    </div>
                </div>
            </form> 
            
            </div>   
            </div>                              
               <!-- <div class="row">
            	<div class="col-sm-12 text-center">
            		<p>Don't have an account? <a href="page-register.html" class="text-primary m-l-5"><b>Sign Up</b></a></p>
                        
                    </div>
            </div>-->
            
        </div>
	
	</body>
	<!-- FOOTER -->
    <?php include('footer.php');?>
</html>