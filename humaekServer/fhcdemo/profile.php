<?php include('configuration/configuration.php');
if(!$_SESSION['EMP_U']){header('Location:index.php');}
$oCompany_details = new AdminClass();
$oUser     = new UserClass();

  $oUser->getStates('', 101);
        $aState   =  $oUser->aResults; 
        $iState   =  $oUser->iResults;  

$oCompany_details->get_emp_details('',$_SESSION['EMP_U']);
$aEmployee_details  = $oCompany_details->aResults;
$iEmployee_details  = $oCompany_details->iResults;
//print_r($aEmployee_details);

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!--   <link rel="icon" href="../../favicon.ico">-->
<title>fhcdemo</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/settings.css" rel="stylesheet">
<link href="css/styleslider.css" rel="stylesheet">
<link href="font-awesome/css/font-awesome.css" rel="stylesheet">
</head>
<body>
<?php include('header.php') ?>
<div class="container">
      <div class="row">
      <div class="col-md-5  toppad  pull-right col-md-offset-3 edit">
           <a href="edit.php">Edit Profile</a>

        <a href="logout.php" >Logout</a>
       <br>
<p class=" text-info"><?= $aEmployee_details[0]['created']; ?> </p>
      </div>
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xs-offset-0 col-sm-offset-0 col-md-offset-2 col-lg-offset-2 toppad" >
   
   
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title"><?= $aEmployee_details[0]['name']; ?></h3>
            </div>
            <div class="panel-body">
             <div class="row">
			  <?php if($aEmployee_details[0]['image']==''){?>
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png" class="img-circle img-responsive"> </div>
			  <?php }else{ ?>
				  
				  <div class="col-md-3 col-lg-3 " align="center"><img src="images/<?php echo $aEmployee_details[0]['image'];?>" class="img-circle img-responsive" ></div>

			<?php  } ?>
             

                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information">
                    <tbody>
	<tr>		
		<td>Employee Name</td> 
		<td><?=$aEmployee_details[0]['name']?></td>	
	</tr>
    
	<tr><td>Email ID</td> 
		<td><?=$aEmployee_details[0]['email']?></td>	
	</tr><tr>	
    	
		<td>Password</td> 
		<td><?=$aEmployee_details[0]['password']?></td>	
	</tr><tr>	
    	
		<td>Mobile number</td> 
		<td><?=$aEmployee_details[0]['phone']?></td>	
	</tr><tr>	
    	
		<td>Total Work Experience</td> 
		<td><?=$aEmployee_details[0]['work_exp_year']?> Year <?=$aEmployee_details[0]['work_exp_month']?> Month</td>	
	</tr><tr>	
    	
		<td>Current Designation</td> 
		<td><?=$aEmployee_details[0]['current_designation']?></td>	
	</tr><tr>	
    	
		<td>Current Company</td> 
		<td><?=$aEmployee_details[0]['current_company']?></td>	
	</tr><tr>	
    	
		<td>Annual Salary</td> 
		<td><?=$aEmployee_details[0]['annual_salary_lakhs']?> Lakhs <?=$aEmployee_details[0]['annual_salary_thousand']?> Thousand</td>	
	</tr><tr>	
    	
		<td>Working since</td> 
		<td><?=$aEmployee_details[0]['working_since_year']?>/<?=$aEmployee_details[0]['working_since_month']?> To <?=$aEmployee_details[0]['working_since_present']?></td>	
	</tr><tr>		
    
		<td>Current Location</td> 
		<td><?=$aEmployee_details[0]['fld_state_name']?>,<?php if($aEmployee_details[0]['fld_city']==0){  
			echo $aEmployee_details[0]['other_city']; }else{
			
			echo $aEmployee_details[0]['fld_city_name'];
		}
		
		
		?></td>	
	</tr><tr>		
   
		<td>Duration of Notice Period</td> 
		<td><?=$aEmployee_details[0]['duration_notice_period']?></td>	
	</tr><tr>		
    
		<td>Skills</td> 
		<td><?=$aEmployee_details[0]['skills']?></td>	
	</tr><tr>
    		
		<td>Highest Qualification</td> 
		<td><?=$aEmployee_details[0]['highest_qualification']?></td>	
	</tr><tr>	
    	
		<td>Course</td> 
		<td><?=$aEmployee_details[0]['course_name']?></td>	
	</tr><tr>	
    	
		<td>Specialization</td> 
		<td><?=$aEmployee_details[0]['specialization']?></td>	
	</tr><tr>	
    	
		<td>University/College</td> 
		<td><?=$aEmployee_details[0]['university_name']?></td>	
	</tr><tr>	
    	
		<td>Course Duration</td> 
		<td><?=$aEmployee_details[0]['from_date']?></td>To<td><?=$aEmployee_details[0]['to_date']?></td>	
	</tr><tr>	
    	
		<td>Passing Year</td> 
		<td><?=$aEmployee_details[0]['passing_year']?></td>	
	</tr>
                     
                    </tbody>
                  </table>
                  
                  
                </div>
              </div>
            </div>
                 
            
          </div>
        </div>
      </div>
    </div>
    
	<?php require_once('footer.php'); ?>  
    </body>
</html>
<style>

.edit {
  padding-top: 92px;
}
</style>