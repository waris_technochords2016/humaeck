<?php
class GeneralClass extends DBClass
{ 
	function get_records($_sTableName, $_sSelectField, $_sMatchField, $_sMatchFieldValue='')
	{
		 $_sSelectQuery = "SELECT $_sSelectField FROM $_sTableName WHERE 1 = 1";
		if($_sMatchFieldValue != "")
		{
			$_sSelectQuery.= " AND $_sMatchField = '".$_sMatchFieldValue."'";
		}	
		
		$this->execute_select_query($_sSelectQuery);
		$this->aRowData = $this->aQueryData;
		
		return $this->aRowData[0];
	}
	
		function get_totals($_sTableName, $_sSelectField, $_sMatchField='', $_sMatchFieldValue='',$group_by='')
	{
		$_sSelectQuery = "SELECT SUM($_sSelectField) FROM $_sTableName WHERE 1 = 1";
		if($_sMatchFieldValue != "")
		{
			$_sSelectQuery.= " AND $_sMatchField = '".$_sMatchFieldValue."'";
		}
		if($group_by != "")
		{
			$_sSelectQuery.= " GROUP BY '".$group_by."'";
		}
		//SELECT SUM(amount) FROM `orders` where sess_id='db06aff0aa3057fe17c570eb016a7c23' GROUP BY sess_id;	
		$this->execute_select_query($_sSelectQuery);
		$this->aRowData = $this->aQueryData;
		return $this->aRowData[0][0];
	}
	function update_data($_sTableName, $_sMatchFld, $_aData, $_iRecId, $md5='') 
	{
		$fld_str = "";
		$val_str = "";
		if($_sTableName && is_array($_aData))
		{	
			$_sSelectQuery = "SHOW COLUMNS FROM `$_sTableName`";
			$this->execute_select_query($_sSelectQuery);
			$this->aColoumData = $this->aQueryData;
			$this->iColoumData = $this->iTotalRecords;
			$this->sError = $this->sQueryError;
			$this->sResult = $this->sQueryResult;
			for($i=0; $i<$this->iColoumData; $i++)
			{
				$aDataArray[] = $this->aColoumData[$i]['Field'];
			}
			foreach($_aData as $key=>$val)
			{	
				if(in_array($key, $aDataArray))
				{
					$fld_str.="$key,";	
					if($val=='now()')
					{	
						$val_str.= "$key=".trim($val).",";
					}	
					else
					{
						$val_str.="$key='".trim($val)."',";
					}	
				}
			}
			$val_str=substr($val_str,0,-1);
			if($md5!='') $_sMatchFld='MD5('.$_sMatchFld.')';
		     $_sUpdateQuery = "UPDATE $_sTableName SET $val_str WHERE ".$_sMatchFld." IN (".$_iRecId.") ";
			
		    $_sUpdateQuery = str_replace("'on'", "'1'", $_sUpdateQuery);
			//echo $_sUpdateQuery;
			$this->execute_update_query($_sUpdateQuery);
			return mysqli_affected_rows($this->oConn);
		}
	}
	
	function insert_data($_sTableName,$_aData) 
	{
		$fld_str='';
		$val_str='';
		if($_sTableName && is_array($_aData) && !empty($_aData))
		{
			$_sSelectQuery="SHOW COLUMNS FROM `$_sTableName`";
			$this->execute_select_query($_sSelectQuery);
			$this->aColoumData = $this->aQueryData;
			$this->iColoumData = $this->iTotalRecords;
			$this->sError = $this->sQueryError;
			$this->sResult = $this->sQueryResult;
			for($i=0; $i<$this->iColoumData; $i++)
			{
				$aDataArray[] = $this->aColoumData[$i]['Field'];
			}
			foreach($_aData as $key=>$val)
			{	
				if(in_array($key, $aDataArray))
				{
					$fld_str.="$key,";	
					if($val=='now()')
					{	
						$val_str.= trim($val).",";
					}	
					else
					{
						$val_str.="'".trim($val)."',";
					}	
				}
			}
			$fld_str=substr($fld_str,0,-1);
			$val_str=substr($val_str,0,-1);
			 $_sInsertQuery=" INSERT INTO $_sTableName ($fld_str) VALUES ($val_str)";
			//echo $_sInsertQuery;
			$this->execute_select_query($_sInsertQuery);
			//return mysqli_insert_id();
		}
	}
	
	function check_duplicate_record($_tablName,$_matchfield,$_rRecordId,$_match_field_old='',$_rRecordId_old='')
	{
		$selQuery="SELECT * FROM $_tablName WHERE $_matchfield='".$_rRecordId."'";
		if($_match_field_old!='')
		{
			$selQuery.="AND $_match_field_old <>".$_rRecordId_old;
		}
		//echo $selQuery;
		$this->execute_select_query($selQuery);
		$this->acount = $this->aQueryData;
		$this->icount = $this->iTotalRecords;
	}
		function check_user_login($_tablName,$_matchfield,$_rRecordId,$_match_field_old='',$_rRecordId_old='')
	{
		$selQuery="SELECT * FROM $_tablName WHERE $_matchfield='".$_rRecordId."'";
		if($_match_field_old !='')
		{		
		   $selQuery.=" and $_match_field_old='".$_rRecordId_old."'";
		 }
	    //echo $selQuery;
		//exit();
		$this->execute_select_query($selQuery);
		$this->acount = $this->aQueryData;
		$this->icount = $this->iTotalRecords;
	}
	
	function upload_files($_aAllowedExtension, $_aFile, $_sDestinationPath, $_sNewFileName, $_size=5242880)
	{
	   /* print_r($_aAllowedExtension);
		echo'<pre>';
		print_r($_aFile);
		echo $_sDestinationPath;
		echo $_sNewFileName;
		exit();*/
		$ext=explode('.', $_aFile['name']);  //Get the extention of File 
		$ext=strtolower(end($ext));	//To get the extention and change it lower case
		if($_aFile['size']<$_size && $_aFile['size']!=0)  //Check the size of File
		{
			if(is_array($_aAllowedExtension) ? (in_array($ext, $_aAllowedExtension)) : false)
			{
				$sDbFileName = $_sNewFileName.".".$ext;
				if(move_uploaded_file($_aFile['tmp_name'], $_sDestinationPath.'/'.$sDbFileName)):
				chmod($_sDestinationPath.'/'.$sDbFileName, 0777);
				$sMessege = "0";    //File has been uploaded Successfulley
				else:
				$sMessege = "26";    //There is a problem with the File uploading please try Again
				endif;
			}
			else
			{
				$sMessege = "27";    //Please select allowed extension
			}			
		}
		else 
		{
			$sMessege = "28";    //Please select a File to Upload
		}
		return $sMessege;
	}

	function delete_record($_tablName,$_matchfield,$_rRecordId)
	{
		$delQuery="DELETE FROM $_tablName WHERE $_matchfield IN (".$_rRecordId.") ";
		
		$this->execute_delete_query($delQuery);
		$this->sResult =$this->sQueryResult ;
	}
	
	function changeUserStatus($tableName ,$id , $fldName , $status){

    if($status==1){
      $sSqlResults="UPDATE $tableName SET `status`= '0' WHERE $fldName=$id";
	  //echo $sSqlResults;
	  	$this->execute_update_query($sSqlResults);
	}
	else{
      $sSqlResults="UPDATE $tableName SET `status`= '1' WHERE $fldName=$id";
     //   echo $sSqlResults;
	  	$this->execute_update_query($sSqlResults);
	}
	}
/*	
	
	function date_format($_sDate)
	{
		$aTmpDate = explode(" ", $_sDate);
		if($aTmpDate[0] != '')
		{
			$aNewTmpDate = explode("-", $aTmpDate[0]);
			$sNewDate = date("M d Y", mktime(0, 0, 0, $aNewTmpDate[1], $aNewTmpDate[2], $aNewTmpDate[0]));;
		}
		return $sNewDate;
	}*/
}
?>