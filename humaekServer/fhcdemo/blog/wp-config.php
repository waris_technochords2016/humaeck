<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'atest8ov_fhcblog');

/** MySQL database username */
define('DB_USER', 'atest8ov_fhcblog');

/** MySQL database password */
define('DB_PASSWORD', '1q2w3e!Q@W#E');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'VU Wo92]` 3J4EFEBu@jWa0(~7rv[EM%@*3Lc34mRl]%CrSr>Dq3|*Nl~XUYxw(|');
define('SECURE_AUTH_KEY',  'Sk!8{%dw+og{Q.rZJ8gj&F,!{L-TQ!U4F(tC(VOX3l,Ij;_;su=Bo_?GE{a^,<*1');
define('LOGGED_IN_KEY',    '_! HUpL?+1{YUXN=c# ak5{TE^r@dBX9C[C8Vdo:5m<iK9^crqcIe/7_Uc>:GiMe');
define('NONCE_KEY',        'f$wqRL|:X{AK$#b,ws8U-8AN9d84]Z&K(2hKpE?wLN;gMlR{5`K91:?ms4{_}hqC');
define('AUTH_SALT',        '|wnWp/PsO+&eLE&w;h.o.&/3ZL=D%^OPsu]F>gz]Smc?`}gS}:j^TxsBN)y&gL?z');
define('SECURE_AUTH_SALT', 'p:5[]8if7fpVh<YwO8T~Yc }qN(#C&CD:uVMR0K~/l=Mn=,2IBv,,*mC|c2EjkX1');
define('LOGGED_IN_SALT',   'OrR:[wVNl.@;J6=etIZ^xK[@`zI)XiPmg=SE1:4bO$DA=uTo)=n_r]2|jZ0y[eat');
define('NONCE_SALT',       '<BMzmRNh=a3j#?NRLD NGkCK{qyMpCnV21S!As5vpR;fU4]m(zUOSPE}: Tf__|@');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
