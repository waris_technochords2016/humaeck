<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.png">
        <title>Humaeck | Job Portal</title>
        <meta name="author" content="Codrops" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" type="text/css">
        <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="/css/fileinput.min.css" rel="stylesheet" type="text/css">
        <link href="/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="css/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />

        <link href="/css/bootstrap-wysiwyg.min.css" rel="stylesheet" type="text/css">
        <link href="/css/profession-black-green.css" rel="stylesheet" type="text/css" id="style-primary">
        <link href="/css/blocks.css" rel="stylesheet" type="text/css">
        <!-- <link href="/css/app.css" rel="stylesheet" type="text/css"> -->
        <link href="/css/hexagons.css" rel="stylesheet" type="text/css">
        <link href="/css/hexagons.min.css" rel="stylesheet" type="text/css">
        <link href="/css/line-icons.css" rel="stylesheet" type="text/css">


    </head>
    <body class="hero-content-dark footer-dark">
        <div class="page-wrapper">
            <div class="header-wrapper">
            <div class="">
                <div class="header-top">
                    <div class="container">
                        <div class="header-brand">
                            <div class="header-logo">
                                <a href="/">
                                    <img src="/images/logo-2.png" alt="Logo">
                                </a>
                            </div><!-- /.header-logo-->
                        </div><!-- /.header-brand -->

                        <!-- <ul class="header-actions nav nav-pills">
                            <li><a href="javascript:void(0);">Login</a></li>
                            <li><a href="javascript:void(0);">Sign Up</a></li>
                            <li><a href="javascript:void(0);" class="primary">Create Resume</a></li>
                        </ul> --><!-- /.header-actions -->

                        <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".header-nav">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div><!-- /.container -->
                </div><!-- /.header-top -->

                <div class="header-bottom">
                    <div class="container">
                        <ul class="header-nav nav nav-pills collapse">
                            <li>
                                <a href="/">HOME</a>
                            </li>

                            <li >
                                <a href="about-us">ABOUT US <i class=""></i></a>
                            </li>

                            <li class="active">
                                <a href="services">SERVICES <i class=""></i> </a>
                            </li>

                            <li >
                                <a href="contact-us">CONTACT US <i class=""></i></a>
                                <!-- <ul class="sub-menu">
                                    <li><a href="candidates.html">Candidates List</a></li>
                                    <li><a href="resume.html">Resume</a></li>
                                    <li><a href="create-resume.html">Create Resume</a></li>
                                </ul> --><!-- /.sub-menu -->
                            </li>
                        </ul>

                        <!-- <div class="header-search hidden-sm">
                            <form method="get" action="?">
                                <input type="text" class="form-control" placeholder="Search ...">
                            </form>
                        </div> --><!-- /.header-search -->
                    </div><!-- /.container -->
                </div><!-- /.header-bottom -->
            </div><!-- /.header -->
        </div><!-- /.header-wrapper-->

        <div class="container-fluide">
            <div id="splitlayout" class="splitlayout">
                <div class="intro">
                    <div class="side side-left">
                        <!-- <header class="codropsheader clearfix">
                            <a href="index.php"><img src="/images/Final.png" alt="Logo" class="pull-left" width="235px;"></a>
                            <a href="index.php"><button class="btn btn-warning margin" type="button"><i class="fa fa-bell-o"></i> Home</button></a>
                        </header> -->
                        <div class="intro-content">
                            <div class="profile"><img src="/images/client.png" alt="profile1"></div>
                            <h2><span>Client services </span></h2>
                        </div>
                        <div class="overlay"></div>
                    </div>
                    <div class="side side-right">
                        <div class="intro-content">
                            <div class="profile"><img src="/images/candidate.png" alt="profile2"></div>
                            <h2><span>Candidate services</span></h2>
                        </div>
                        <div class="overlay"></div>
                    </div>
                </div><!-- /intro -->
                <div class="page page-right">
                    <div class="page-inner">
                         <section>
                            <h3>Candidate services</h3>
                            <p>1. We invite talented professionals to sign up and register with us and apply for the open vacancies, based on the information provided by our clients.</p>
                              <p>2. We personally take each profile into consideration and suggest the best suited jobs for them, with salary matching to the industry standards.</p>
                              <p>3. We verify the company details to make sure that the candidate is going in a registered company for work.</p>
                              <p>4. We also provide end to end support to our candidates, even after their recruitment process is complete.</p>
                               <div class="inner contact">
                                <!-- Form Area -->
                                <h3>Get In Touch</h3>
                                <div class="contact-form">
                                    <!-- Form -->
                                    <form id="contact-us" method="post" action="servicemailer.php">
                                        <!-- Left Inputs -->
                                        <div class="col-xs-6 wow animated slideInLeft" data-wow-delay=".5s">
                                            <!-- Name -->
                                            <input type="text" name="firstname" id="name" required="required" class="form" placeholder="Name" />
                                            <!-- Email -->
                                            <input type="email" name="email" id="mail" required="required" class="form" placeholder="Email" />
                                            <!-- Subject -->
                                            <input type="text" name="subject" id="subject" required="required" class="form" placeholder="Subject" />
                                        </div><!-- End Left Inputs -->
                                        <!-- Right Inputs -->
                                        <div class="col-xs-6 wow animated slideInRight" data-wow-delay=".5s">
                                            <!-- Message -->
                                            <textarea name="message" id="message" class="form textarea"  placeholder="Message"></textarea>
                                            <input id="phone" name="phone" type="text" placeholder="Phone" class="form">
                                        </div><!-- End Right Inputs -->
                                        <!-- Bottom Submit -->
                                        <div class="relative col-xs-12 col-md-10 col-lg-10 col-md-push-1 col-lg-push-1">
                                            <!-- Send Button -->
                                            <button type="submit" id="submit" name="submit" class="form-btnService semibold">Submit</button>
                                        </div><!-- End Bottom Submit -->
                                        <!-- Clear -->
                                        <div class="clear"></div>
                                    </form>

                                    <!-- Your Mail Message -->
                                    <div class="mail-message-area">
                                        <!-- Message -->
                                        <div class="alert gray-bg mail-message not-visible-message">
                                            <strong>Thank You !</strong> Your email has been delivered.
                                        </div>
                                    </div>

                                </div><!-- End Contact Form Area -->
                            </div><!-- End Inner -->
                        </section>
                    </div><!-- /page-inner -->
                </div><!-- /page-right -->
                <div class="page page-left">
                    <div class="page-inner">
                         <section>
                            <h3>Client services</h3>
                            <p>1. Humaeck, as an HR consultancy, aims to find the best-suited professionals for our clients.
                              <p>2. We take it as our sacred duty to go through all the steps rigorously, inviting candidates’ resumes, review their profiles, shortlist them, call them for personal interview, for which we employ an expert of that field, and finally send the best candidates to the client for final interview. </p>
                              <p>3. We provide value added services like candidate’s documents verification and preparing appointment documents, ready to sign.</p>
                              <p>4. We also provide the after-service support to our clients.</p>

                           </p>
                            <div class="inner contact">
                                <!-- Form Area -->
                                <h3>Get In Touch</h3>
                                <div class="contact-form">
                                    <!-- Form -->
                                    <form id="contact-us" method="post" action="servicemailer.php">
                                        <!-- Left Inputs -->
                                        <div class="col-xs-6 wow animated slideInLeft" data-wow-delay=".5s">
                                            <!-- Name -->
                                            <input type="text" name="firstname" id="name" required="required" class="form" placeholder="Name" />
                                            <!-- Email -->
                                            <input type="email" name="email" id="mail" required="required" class="form" placeholder="Email" />
                                            <!-- Subject -->
                                            <input type="text" name="subject" id="subject" required="required" class="form" placeholder="Subject" />
                                        </div><!-- End Left Inputs -->
                                        <!-- Right Inputs -->
                                        <div class="col-xs-6 wow animated slideInRight" data-wow-delay=".5s">
                                            <!-- Message -->
                                            <textarea name="message" id="message" class="form textarea"  placeholder="Message"></textarea>
                                            <input id="phone" name="phone" type="text" placeholder="Phone" class="form">
                                        </div><!-- End Right Inputs -->
                                        <!-- Bottom Submit -->
                                        <div class="relative col-xs-12 col-md-10 col-lg-10 col-md-push-1 col-lg-push-1">
                                            <!-- Send Button -->
                                            <button type="submit" id="submit" name="submit" class="form-btnService semibold">Submit</button>
                                        </div><!-- End Bottom Submit -->
                                        <!-- Clear -->
                                        <div class="clear"></div>
                                    </form>

                                    <!-- Your Mail Message -->
                                    <div class="mail-message-area">
                                        <!-- Message -->
                                        <div class="alert gray-bg mail-message not-visible-message">
                                            <strong>Thank You !</strong> Your email has been delivered.
                                        </div>
                                    </div>

                                </div><!-- End Contact Form Area -->
                            </div><!-- End Inner -->
                        </section>
                    </div><!-- /page-inner -->
                </div><!-- /page-left -->
                <a href="#" class="back back-right" title="back to intro">&rarr;</a>
                <a href="#" class="back back-left" title="back to intro">&larr;</a>
            </div><!-- /splitlayout -->
        </div><!-- /container -->


</div><!-- /.page-wrapper -->

<script src="/js/classie.js"></script>
<script src="/js/modernizr.custom.js"></script>
<script src="/js/cbpSplitLayout.js"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.ezmark.js"></script>

<script type="text/javascript" src="/js/collapse.js"></script>
<script type="text/javascript" src="/js/dropdown.js"></script>
<script type="text/javascript" src="/js/tab.js"></script>
<script type="text/javascript" src="/js/transition.js"></script>
<script type="text/javascript" src="/js/fileinput.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-wysiwyg.min.js"></script>
<script type="text/javascript" src="/js/jquery.cycle2.min.js"></script>
<script type="text/javascript" src="/js/jquery.cycle2.carousel.min.js"></script>
<script type="text/javascript" src="/js/countup.min.js"></script>
<script type="text/javascript" src="/js/profession.js"></script>

<script type="text/javascript" src="/js/hexagons.js"></script>
<script type="text/javascript" src="/js/hexagons.min.js"></script>

</body>
</html>
