<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.png">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" type="text/css">
    <link href="/css/style.css" rel="stylesheet" type="text/css">
    <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/css/fileinput.min.css" rel="stylesheet" type="text/css">
    <link href="/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
    <link href="/css/bootstrap-wysiwyg.min.css" rel="stylesheet" type="text/css">
    <link href="/css/profession-black-green.css" rel="stylesheet" type="text/css" id="style-primary">
    <link href="/css/app.css" rel="stylesheet" type="text/css" id="style-primary">
    <link rel="shortcut icon" type="image/x-icon" href="assets/favicon.png">
    <link href="/css/hexagons.css" rel="stylesheet" type="text/css">
    <link href="/css/hexagons.min.css" rel="stylesheet" type="text/css">

    <title>Humaeck | Job Portal</title>
</head>


<body class="hero-content-dark footer-dark">

<div class="page-wrapper">
    <div class="header-wrapper">
    <div class="">
        <div class="header-top">
            <div class="container">
                <div class="header-brand">
                    <div class="header-logo">
                        <a href="/">
                            <img src="/images/logo-2.png" alt="Logo">
                        </a>
                    </div><!-- /.header-logo-->
                </div><!-- /.header-brand -->

                <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".header-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div><!-- /.container -->
        </div><!-- /.header-top -->

        <div class="header-bottom">
            <div class="container">
                <ul class="header-nav nav nav-pills collapse">
                    <li class="active">
                        <a href="/">HOME</a>
                    </li>

                    <li >
                        <a href="about-us">ABOUT US <i class=""></i></a>
                    </li>

                    <li >
                        <a href="services">SERVICES <i class=""></i> </a>
                    </li>

                    <li >
                        <a href="contact-us">CONTACT US <i class=""></i></a>
                        <!-- <ul class="sub-menu">
                            <li><a href="test.html">Candidates List</a></li>
                            <li><a href="javascript:void(0);">Resume</a></li>
                            <li><a href="create-javascript:void(0);">Create Resume</a></li>
                        </ul> --> <!-- /.sub-menu -->
                    </li>
                </ul>

               <!--  <div class="header-search hidden-sm">
                    <form method="get" action="?">
                        <input type="text" class="form-control" placeholder="Search ...">
                    </form>
                </div> --><!-- /.header-search -->
            </div><!-- /.container -->
        </div><!-- /.header-bottom -->
    </div><!-- /.header -->
</div><!-- /.header-wrapper-->


    <div class="main-wrapper">
        <div class="main">
            <div class="hero-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-md-offset-2 col-md-8 col-md-offset-2">
                <h1 class="center">New approach to <strong class="text-primary">Recruiting</strong></h1>
                <h2 class="text-center">Hot spot for job seekers and employers.</h2>
                <!-- <a href="create-javascript:void(0);" class="hero-content-action pull-center">Upload Resume</a> -->
            </div><!-- /.col-* -->

            <div>
                <div class="hero-content-carousel">
                    <!-- <h2>Happening Right Now</h2> -->

                    <!-- <ul class="cycle-slideshow vertical"
                        data-cycle-fx="carousel"
                        data-cycle-slides="li"
                        data-cycle-carousel-visible="7"
                        data-cycle-carousel-vertical="true">
                        <li><a href="javascript:void(0);">Dropbox</a> is looking for UX/UI designer.</li>
                        <li>Python Developer <a href="javascript:void(0);">John Doe</a>.</li>
                        <li><a href="javascript:void(0);">IT consultant</a> is needed by <a href="javascript:void(0);">Twitter</a>.</li>
                        <li>Project manager wanted for <a href="javascript:void(0);">e-shop portal</a>.</li>
                        <li><a href="javascript:void(0);">Mark Peterson</a> needs to fix his website.</li>
                        <li><a href="javascript:void(0);">Facebook</a> is looking for <a href="javascript:void(0);">beta testers</a>.</li>
                        <li><a href="javascript:void(0);">Instagram</a> needs help with new API.</li>
                        <li><a href="javascript:void(0);">Dropbox</a> is looking for UX/UI designer.</li>
                        <li>Python Developer <a href="javascript:void(0);">John Doe</a> is looking for work.</li>
                        <li><a href="javascript:void(0);">IT consultant</a> is needed by <a href="javascript:void(0);">Twitter</a>.</li>
                        <li>Project manager wanted for one time <a href="javascript:void(0);">e-shop portal</a>.</li>
                        <li><a href="javascript:void(0);">Mark Peterson</a> needs to fix his website.</li>
                        <li><a href="javascript:void(0);">Facebook</a> is looking for <a href="javascript:void(0);">beta testers</a>.</li>
                        <li><a href="javascript:void(0);">Instagram</a> needs help with new API.</li>
                    </ul> -->

                    <!-- <a href="javascript:void(0);" class="hero-content-show-all">Show All</a> -->
                </div><!-- /.hero-content-content -->
            </div><!-- /.col-* -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.hero-content -->


<div class="stats">
    <div class="container">
        <div class="row">
            <div class="stat-item col-sm-4" data-to="6">
                <strong id="stat-item-1">6</strong>
                <span>Team Size</span>
            </div><!-- /.col-* -->

            <div class="stat-item col-sm-4" data-to="187,432">
                <strong id="stat-item-2">187,432</strong>
                <span>Active Resumes</span>
            </div><!-- /.col-* -->

            <div class="stat-item col-sm-4" data-to="0">
                <strong id="stat-item-3">0</strong>
                <span>Projects Completed</span>
            </div><!-- /.col-* -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.stats -->



<div class="container">
	<div class="panels-highlighted">
    <div class="row">
        <div class="panel-highlighted-wrapper col-sm-6">
            <div class="panel-highlighted-inner panel-highlighted-secondary">
                <h2>Hire an employee</h2>

                <p>We want to make Hiring a streamlined process for you by customizing our services specifically to your business while reducing the time and cost of Hire.
                </p>

                <!-- <a href="javascript:void(0);" class="btn btn-white">Sign up as company</a> -->
            </div><!-- /.panel-inner -->
        </div><!-- /.panel-wrapper -->

        <div class="panel-highlighted-wrapper col-sm-6">
            <div class="panel-highlighted-inner panel-highlighted-primary panel">
                <h2>Looking for a job</h2>

                <p>
                    Not getting right calls from the right recruiters? Need help with profiling and resume? Or just want some consulting? Don't Hesitate, Contact us. We offer services free for candidates.
                </p>

                <!-- <a href="javascript:void(0);" class="btn btn-white">Sign up as employee</a> -->
            </div><!-- /.panel-inner -->
        </div><!-- /.panel-wrapper -->
    </div><!-- /.row-->
</div><!-- /.panels -->


	<div class="page-title">
    <h2>Who We Are</h2>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <p>
                We are Human Resource Consultants, providing an out of box solution for Recruitment and Staffing. We reduce the time for a hire drastically with our Pro-active Model, which not only assure you a Quality Talent, but also provides it consistently at much rapid pace, so your business does not suffer.
            </p>
        </div><!-- /.col-* -->
    </div><!-- /.row -->
</div><!-- /.page-title -->

<div class="posts">
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="post-box">
                <div class="post-box-image">
                    <a href="">
                        <img src="/images/blog-1.jpg" alt="">
                    </a>
                </div><!-- /.post-box-image -->

                <div class="post-box-content">
                    <h2><a href="javascript:void(0);">Our Services</a></h2>

                    <p>
                       Our offerings cover a range of services for our Clients, primarily Recruitment and Staffing. Each of the primary services has multiple services clubbed into one package to offer maximum value at best price.
                    </p>

                    <a href="aboutUs.php" target="_blank" class="post-box-read-more">Read More</a>
                </div><!-- /.post-box-content -->
            </div><!-- /.post-box -->
        </div><!-- /.col-sm-6 -->

        <div class="col-sm-12 col-md-6">
            <div class="post-box post-box-small">
                <div class="post-box-image">
                    <a href="javascript:void(0);">
                        <img src="/images/blog-2.jpg" alt="">
                    </a>
                </div><!-- /.post-box-image -->

                <div class="post-box-content">
                    <h2><a href="javascript:void(0);">Recruitment</a></h2>

                    <p>
                        Recruitment service  includes Verification, On-boarding &amp; more...
                    </p>
                </div><!-- /.post-box-content -->
            </div><!-- /.post-box -->

            <div class="post-box post-box-small">
                <div class="post-box-image">
                    <a href="javascript:void(0);">
                        <img src="/images/blog-3.jpg" alt="">
                    </a>
                </div><!-- /.post-box-image -->

                <div class="post-box-content">
                    <h2><a href="javascript:void(0);">Staffing</a></h2>

                    <p>
                        We provide staffing solutions for small to mid-size businesses.
                    </p>
                </div><!-- /.post-box-content -->
            </div><!-- /.post-box -->

            <div class="post-box post-box-small">
                <div class="post-box-image">
                    <a href="javascript:void(0);">
                        <img src="/images/blog-4.jpg" alt="">
                    </a>
                </div><!-- /.post-box-image -->

                <div class="post-box-content">
                    <h2><a href="javascript:void(0);">Candidates Services</a></h2>

                    <p>
                        We have range of services for candidates for free.
                    </p>
                </div><!-- /.post-box-content -->
            </div><!-- /.post-box -->


        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->
</div><!-- /.posts-->


	<div class="panels">
    <div class="row">
        <div class="panel-wrapper col-sm-4">
            <div class="panel-inner">
                <h2>Why Humaeck?</h2>

                <ul>
                    <li><i class=""></i>Proactive &amp; Responsive Business Model</li>
                    <li><i class=""></i>Rapid Delivery with Quality Talent</li>
                    <li><i class=""></i>Don't Hesitate and Contact Us.</li>
                </ul>
            </div><!-- /.panel-inner -->
        </div><!-- /.panel-wrapper -->

        <div class="panel-wrapper col-sm-4">
            <div class="panel-inner">
                <h2>Reach out to us</h2>

                <ul>
                    <li><i class=""></i>We would love to hear from you</li>
                    <li><i class=""></i>Talk to us +91-8879737497 or</li>
                    <li><i class=""></i>Let's meet over a coffee!</li>
                </ul>
            </div><!-- /.panel-inner -->
        </div><!-- /.panel-wrapper -->

        <div class="panel-wrapper col-sm-4">
            <div class="panel-inner">
                <h2>Know us more ?</h2>

                <p>Get our profile here. Drop us a query, we  would love to chat with you!</p>
                <a href="Humaeck.pdf" class="panel-show-more" target="blank">Get Profile</a>
            </div><!-- /.panel-inner -->
        </div><!-- /.panel-wrapper -->
    </div><!-- /.row -->
</div><!-- /.panels -->

	<div class="block background-secondary fullwidth candidate-title">
    <div class="page-title">
        <h2>Our Clients</h2>

        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <p>
                    Shucks! It's Empty. Give us a chance, we are sure you would love working with us. Just give us a call on +91-8879 73 7497 or drop us a mail @ hrsupport@humaeck.com or send us a query. We are waiting to feature you here!!
                </p>
            </div><!-- /.col-* -->
        </div><!-- /.row -->
    </div><!-- /.page-title -->
</div><!-- /.fullwidth -->

<div class="row mt-60">
    <div class="candidate-boxes">

    </div><!-- /.candidate-boxes -->
</div><!-- /.row -->

</div><!-- /.container -->



<div class="cta-text">
	<div class="container">
		<div class="cta-text-inner">

<div class="inner contact">
                <!-- Form Area -->
                <div class="contact-form">
                    <!-- Form -->
                    <form id="contact-us" method="post" action="indexmailer.php">
                        <!-- Left Inputs -->
                        <div class="col-xs-6 wow animated slideInLeft" data-wow-delay=".5s">
                            <!-- Name -->
                            <input type="text" name="firstname" id="name" required="required" class="form" placeholder="Name" />
                            <!-- Email -->
                            <input type="email" name="email" id="mail" required="required" class="form" placeholder="Email" />
                            <!-- Subject -->
                            <input type="text" name="subject" id="subject" required="required" class="form" placeholder="Subject" />
                        </div><!-- End Left Inputs -->
                        <!-- Right Inputs -->
                        <div class="col-xs-6 wow animated slideInRight" data-wow-delay=".5s">
                            <!-- Message -->
                            <textarea name="message" id="message" class="form textarea"  placeholder="Message"></textarea>
                            <input id="phone" name="phone" type="text" placeholder="Phone" class="form">
                        </div><!-- End Right Inputs -->
                        <!-- Bottom Submit -->
                        <div class="relative col-xs-12">
                            <!-- Send Button -->
                            <button type="submit" id="submit" name="submit" class="form-btn semibold">Send Message</button>
                        </div><!-- End Bottom Submit -->
                        <!-- Clear -->
                        <div class="clear"></div>
                    </form>

                    <!-- Your Mail Message -->
                    <div class="mail-message-area">
                        <!-- Message -->
                        <div class="alert gray-bg mail-message not-visible-message">
                            <strong>Thank You !</strong> Your email has been delivered.
                        </div>
                    </div>

                </div><!-- End Contact Form Area -->
            </div><!-- End Inner -->

		</div> <!-- /.cta-text-inner -->
	</div>
</div>


        </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

    <div class="footer-wrapper">
    <div class="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="footer-top-block">
                            <h2>Humaeck</h2>
                            <p class="text-justify">
                              “Human resources are like natural resources; they're often buried deep. You have to go looking for them; they're not just lying around on the surface. You have to create the circumstances where they show themselves.”
                           </p>
                           <medium class="pull-right quote"><em> Ken Robinson</em></medium>
                            <!-- <p>
                                Fusce congue, risus et pulvinar cursus, orci arcu tristique lectus, sit amet placerat justo ipsum eu diam. Pellentesque tortor urna, pellentesque nec molestie eget, volutpat in arcu. Maecenas a lectus mollis.
                            </p> -->
                        </div><!-- /.footer-top-block -->
                    </div><!-- /.col-* -->

                    <div class="col-sm-3 col-sm-offset-1">
                        <div class="footer-top-block">
                            <h2>QUICK LINKS</h2>
                            <ul>
                                <li><a href="/" target="_blank">HOME</a></li>
                                <li><a href="about-us" target="_blank">ABOUT US</a></li>
                                <li><a href="services" target="_blank">SERVICES</a></li>
                                <li><a href="contact-us" target="_blank">CONTACT US</a></li>
                            </ul>
                        </div><!-- /.footer-top-block -->
                    </div><!-- /.col-* -->

                    <div class="col-sm-3">
                         <div class="footer-top-block">
                          <h2>SOCIAL NETWORK</h2>
                            <a href="https://www.facebook.com/" target="_blank" class="hb-xs-margin"><span class="hb hb-xs hb-facebook"><i class="fa fa-facebook"></i></span></a>
                            <a href="https://plus.google.com/" class="hb-xs-margin" target="_blank"><span class="hb hb-xs hb-google-plus"><i class="fa fa-google-plus"></i></span></a>
                            <a href="https://twitter.com/" class="hb-xs-margin" target="_blank"><span class="hb hb-xs hb-twitter"><i class="fa fa-twitter"></i></span></a>
                            <a href="https://www.linkedin.com/" class="hb-xs-margin" target="_blank"><span class="hb hb-xs hb-linkedin"><i class="fa fa-linkedin-square"></i></span></a>
                        </div><!-- /.footer-top-left -->
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.footer-top -->

        <div class="footer-bottom">
            <div class="container">
                <div class="footer-bottom-left">
                    &copy; <a href="javascript:void(0);">Humaeck</a>, 2017 All rights reserved.
                </div><!-- /.footer-bottom-left -->

                <div class="footer-bottom-right">
                    Created by <a href="http://technochords.com" target="blank">Technochords</a>
                </div><!-- /.footer-bottom-right -->
            </div><!-- /.container -->
        </div><!-- /.footer-bottom -->
    </div><!-- /.footer -->
</div><!-- /.footer-wrapper -->

</div><!-- /.page-wrapper -->

<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery.ezmark.js"></script>

<script type="text/javascript" src="/js/collapse.js"></script>
<script type="text/javascript" src="/js/dropdown.js"></script>
<script type="text/javascript" src="/js/tab.js"></script>
<script type="text/javascript" src="/js/transition.js"></script>
<script type="text/javascript" src="/js/fileinput.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-wysiwyg.min.js"></script>

<script type="text/javascript" src="/js/jquery.cycle2.min.js"></script>
<script type="text/javascript" src="/js/jquery.cycle2.carousel.min.js"></script>

<script type="text/javascript" src="/js/countup.min.js"></script>

<script type="text/javascript" src="/js/profession.js"></script>
<script type="text/javascript" src="/js/hexagons.js"></script>
<script type="text/javascript" src="/js/hexagons.min.js"></script>
</body>
</html>
